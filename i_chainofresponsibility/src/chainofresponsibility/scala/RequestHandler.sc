val verifyHasName = {data: Map[String, String] =>
  println("verifying has name")
  !data.contains("name")
}

val verifyNameLongerThan3 = {data: Map[String, String] =>
  println("verifying name longer than 3")
  data("name").length <= 3
}

val lastHandler = {data: Map[String, String]=>
  println("finished.")
  true
}
val request = Map("name"->"laozhu")
val request2 = Map("name"->"zhu")
val verifiers = List(verifyHasName, verifyNameLongerThan3, lastHandler)


verifiers.find(_.apply(request))



verifiers.find(_.apply(request2))



