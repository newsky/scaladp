package proxy.scala

class RealImage(val fileName:String) {
  def loadImageFromDisk() {
    // loading from file
  }
  def displayImage() {
    // display
  }
}
class VirtualProxy {
  lazy val img:RealImage = new RealImage("image.png")
}
