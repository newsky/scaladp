trait Impl1 {
  def impl = 1
}

trait Impl2 {
  def impl = 2
}

object X extends Impl1 with Impl2 {
  override def impl = super[Impl1].impl
}

X.impl

