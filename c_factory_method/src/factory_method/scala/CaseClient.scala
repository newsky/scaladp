package factory_method.scala

object CaseClient extends App {
	trait IFruit {
	  def taste: String
	}
	case class Apple(mature: Boolean) extends IFruit {
	  override def taste = if (mature) "good" else "bad"
	}
	
	case class Orange(mature: Boolean) extends IFruit {
	  override def taste = if (mature) "bad" else "good"
	}
	
	println(Apple(mature = true).taste)
	println(Apple(mature = false).taste)
	println(Orange(mature = true).taste)
	println(Orange(mature = false).taste)
}