package factory_method.java;

public interface IFruit {
	public String getTaste();
}
