package factory_method.java;

public class Apple implements IFruit {
	private boolean mature;

	@Override
	public String getTaste() {
		if(mature) 
			return "good";
		else return "bad";
	}

	public Apple(boolean mature) {
		this.mature = mature;
	}
}
